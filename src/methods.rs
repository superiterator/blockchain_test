//mod structs;

use structs::*;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use std::collections::HashSet;
use indexmap::IndexMap;

/// calculate the BlockHash associated with a Block
pub fn hash_of_block(block: &Block) -> BlockHash {
    let mut s = DefaultHasher::new();
    block.number.hash(&mut s);
    block.parent_hash.hash(&mut s);
    s.finish()
}


/// part 1.1
/// A clone of this block is being passed to "block_append" function. This object will be kept intact
/// Block chain is being passed by reference, so whatever happens inside the function will affect it
pub fn block_append(blockchain: &mut BlockChain, mut block: Block, index:Option<&mut Index>)
//    -> Option< &mut BlockChain>
{

    if blockchain.block.number == 0 {
        block.number = 1;
    } else {
        // Monotonically increasing block number
        block.number = blockchain.block.number + 1;
        block.parent_hash = hash_of_block( &blockchain.block );
    }

    // update the key-value store
    blockchain.kvstore.insert(hash_of_block(&block),block);
    blockchain.block = block;


    // Updating the index (only if index has been generated and used)
    match index {
        Some(idx) => idx.kv_Index.insert( hash_of_block(&block),block ),
        None => None

    };

//    Some(blockchain)
}


/// part 1.2
pub fn block_lookup(blockchain: &BlockChain, hash: BlockHash) -> Option<&Block> {

    println!("Block Lookup-Get: {:?}", blockchain.kvstore.get( &hash ) );

    // Pass the Block object, not a reference to it
    blockchain.kvstore.get( &hash )

}


/// part 1.3
pub fn block_common_ancestor(blockchains: &[BlockChain]) -> Option<&Block> {
    println!("Number of blockchains passed: {}",blockchains.len());

    let mut blockchains_lengths = vec![];

    for blockchain in blockchains {
        blockchains_lengths.push( blockchain.kvstore.len() );
    }

    let min_length = *blockchains_lengths.iter().min_by(|x, y| x.partial_cmp(y).unwrap()).unwrap();
    let mut found_hash:u64 = 0;
    let mut found:bool = false;

    //// Loop through items in reverse direction
    for index in (0 .. min_length).rev() {

        let mut hash_set = HashSet::new();

        for blockchain in blockchains {
            hash_set.insert( *blockchain.kvstore.get_index( index ).unwrap().0 );
        }

        if hash_set.len() == 1 {
            println!("Found the common block");
            found_hash = *hash_set.iter().next().unwrap();
            found = true;
            break;
        }
    }

    if found == true { block_lookup(&blockchains[0], found_hash) } else {None}
}


/// part 1.4
impl Index {

    pub fn new() -> Index{
        Index {
            kv_Index : IndexMap::new()
        }
    }

    /// Given a blockchain, create the associated Index structure
    pub fn generate(&mut self, blockchain: &BlockChain) -> &mut Index {
        self.kv_Index = blockchain.kvstore.clone();
        self
    }

    /// Lookup a block in the Blockchain, efficiently
    pub fn lookup(&self, hash: BlockHash) -> Option<Block> {
        match self.kv_Index.get(&hash) {
            Some(blk) => Some(*blk),
            None => None

        }
    }

    /// Check if a specific block hash in the blockchain exists
    pub fn exists(&self, hash: BlockHash) -> bool {
        match &self.kv_Index.get(&hash) {
            Some(_hsh) => true,
            None => false
        }
    }
}