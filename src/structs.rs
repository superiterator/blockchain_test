extern crate indexmap;
use indexmap::IndexMap;


////////////// Type aliases //////////////////
pub type BlockHash = u64;       /// Block Hash
pub type BlockNumber = u32;     /// Block number (monotonically increasing)
//////////////////////////////////////////////

#[derive(Copy, Clone, PartialEq, Eq,Debug)]
pub struct BlockContent { /* block contents omitted */ }

#[derive(Copy, Clone, PartialEq, Eq,Debug)]
pub struct Block {
    pub number: BlockNumber,
    pub parent_hash: BlockHash,
    pub content: BlockContent,
}

#[derive(Clone,Debug)]
pub struct BlockChain {
    pub block:   Block,
    pub kvstore: IndexMap<u64,Block>,
    pub parent:  Option< Box<BlockChain> >
}


pub struct Index {
    pub kv_Index : IndexMap<u64, Block>
}
