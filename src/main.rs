/// Libraries Imports
extern crate indexmap;

mod structs;
mod methods;

use structs::*;
use methods::*;
use indexmap::IndexMap;


/// part 2: Tests
fn main(){

    //////// List of Raw blocks ////////
    let block  = Block {
        number: 0,
        parent_hash: 0,
        content: BlockContent{}
    };

    let block2 = Block {
        number: 0,
        parent_hash: 0,
        content: BlockContent{}
    };

    let block3 = Block {
        number: 0,
        parent_hash: 0,
        content: BlockContent{}
    };

    let block4 = Block {
        number: 0,
        parent_hash: 0,
        content: BlockContent{}
    };

    let block5 = Block {
        number: 0,
        parent_hash: 0,
        content: BlockContent{}
    };
    ///////////////////////////////////



    let mut block_chain = BlockChain {
        block: block, // tip block
        kvstore: IndexMap::new(),
        parent: None
    };

    let mut block_chain2 = BlockChain {
        block: block, // tip block
        kvstore: IndexMap::new(),
        parent: None
    };

    ////// Add Blocks BlockChain 1 ### last parm index equals "None",becuase we don't employ index usage here
    block_append(&mut block_chain, block,  None);
    block_append(&mut block_chain, block2, None);
    block_append(&mut block_chain, block3, None);
    block_append(&mut block_chain, block4, None);

    ////// Add Blocks BlockChain 2 ### last parm index equals "None",becuase we don't employ index usage here
    block_append(&mut block_chain2, block,  None);
    block_append(&mut block_chain2, block2, None);
    block_append(&mut block_chain2, block3, None);
    block_append(&mut block_chain2, block4, None);
    block_append(&mut block_chain2, block5, None);

    println!("Block: {:?}",block);
    println!("Blockchain: {:?}",block_chain);


    ///// Looking up a speicific block by hash //////
    block_lookup(&block_chain, 13614150237287294529);

    //// Creating Other blockchains based on the previous ones
    let bc1 = block_chain.clone();
    let bc2 = block_chain2.clone();

    //// Finds the youngest common ancestor in the blockchains array
    block_common_ancestor( &[bc1,bc2] );


    ////////////////// Testing Index struct //////////////////
    let mut index = Index::new();

    let mut bchain = block_chain.clone();

    //// Generating Index based  ////
    index.generate(&bchain);

    //// Appending blocks to block chain "bchain"
    //// On each appen
    block_append(&mut bchain, block,  Some(&mut index));
    block_append(&mut bchain, block2, Some(&mut index));
    block_append(&mut bchain, block3, Some(&mut index));
    block_append(&mut bchain, block4, Some(&mut index));


    println!("Debug bchain: {:?}",bchain);

    println!("Exists: {}", index.exists(8722844347853890608) );

}
