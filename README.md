**Blockchain Solution Design**
=

**Notes:**

- To run this application, you will need to issue this command from within app directory: `cargo run`

- This application is ready to be packaged as a binary application. Run `cargo build --release` from application directory

- The main.rs file holds all the intended tests

- Some functions signatures have been changed to comply with some Rust compiler complaints

- `block_append` function in `methods.rs` module, doesn't return a blockchain object/struct, it modifies the one that's passed (blockchain is passed by reference)

- `IndexMap` from the external create `indexmap` has been used instead of `HashMap` in Rust's standard library, because it allows ordering and can act like a stack, and at the same time allows O(1) access complexity to any of its keys. Similar thing could have been done by hand, but why should we if we already have an active crate that does that for us.

- When there is in-memory search, HashMaps is my first go to option, since it allows O(1) access time complexity. That's why there's a similarity between step 1.2 and 1.4, with the exception that step 1.4 employs external index rather than an internal blockchain index as I did in 1.2

---------------------

**Final Note:**

- All code snippets provided by IOHK's team, has been taken as a guideline to my solution, so, there might be minor changes in functions' and structs' signatures compared to the ones suggested.